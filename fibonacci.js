//display fibonacci series
/* 
	0, 1, 1, 2, 3, 5, 8, 13, 21, 34
*/

// write down a function that take a number and return that numbers of fibonacci series
function fibonacci(n) {
	let result = [0, 1];
  for (let i = 0; i < n-2; i++) {
  	result.push((result.at(result.length-2)) + (result.at(result.length-1))); 
  }
  return result.toString();
}

console.log(fibonacci(15));

// write down a function that take a number and return that fibonacci series upto that number
function uptoFibonacci(n) {
	let result = [0, 1];
  if (n < 0) {
  	return "invalid";
  } else if (n == 0) {
  	return 0;
  }
  while ((result.at(result.length-2)) + (result.at(result.length-1)) <= n) {
    result.push((result.at(result.length-2)) + (result.at(result.length-1))); 
  }
  return result.toString();
}

console.log(uptoFibonacci(89));


