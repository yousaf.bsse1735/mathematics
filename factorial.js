/* 
  function for factorial
  let's find factorial of 8
  1*2*3*4*5*6*7*8 = 40320
*/

function factorial(n) {
	let result = 1;
	if (n < 0) {
  	return "invalid";
  } else if (n == 0) {
  	return 1;
  }
	for (let i = 1; i <= n; i++) {
  	result = result * i;
  }
  return result;
}
console.log(factorial(8));



