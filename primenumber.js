// Prime number
/* 
  function for prime numbers
  list first 15 prime numbers
*/

function primeNumbers(n) {
	let number = 2;
	let result = [];
  
	if (n <= 0) {
  	return "invalid";
  }
  
  while (result.length < n) {
  	let isPrime=true;
    
    for (let i=2; i <= Math.ceil(number/2); i++) {
    	if ((number%i) == 0 && i != number && i != 1) {
      	isPrime = false;
      }
    }
    
    if (isPrime) {
    	result.push(number);
    }
    
  	number++;
  }
	
  return result;
}

/* Take a number as argument and return boolean */
function isPrimeNumber(number) {
	let isPrime = true;
  
  if (number < 1) {
  	return "invalid";
  } else if (number == 1) {
  	return "1 is neither prime nor composite";
  }
  
  for (let i=2; i <= Math.ceil(number/2); i++) {
    if ((number%i) == 0 && i != number && i != 1) {
      isPrime = false;
    }
  }
  
  return isPrime;
}

console.log(primeNumbers(15));
console.log(isPrimeNumber(39));



